#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
'''
 * Copyright (C) 2009-2016, Mozbugbox
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301,
 * USA.

Classes:
    CropPixbuf: Crop Pixbuf Dialog
'''

from __future__ import print_function, unicode_literals, absolute_import, division

import sys
import os
import io
import logging as log
import enum

# fitz has to be imported before Gtk, Gdk. Else segfault happens
import fitz
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk, Gdk, GdkPixbuf
import cairo

NATIVE=sys.getfilesystemencoding()

class Response(enum.IntEnum):
    CLEAR = 1

class LineNum(enum.IntEnum):
    H0 = 1 << 0
    H1 = 1 << 1
    V0 = 1 << 2
    V1 = 1 << 3

class DragInfo:
    __slots__ = ["updated", "xy"]
    def __init__(self):
        self.updated = False
        self.xy = None

class CropPixbuf(Gtk.Dialog):
    """Dialog to crop a pixbuf image"""
    def __init__(self, title="Crop Image", parent=None):
        Gtk.Dialog.__init__(self, title, parent,
                Gtk.DialogFlags.USE_HEADER_BAR, (
                    Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                    Gtk.STOCK_CLEAR, Response.CLEAR,
                    Gtk.STOCK_OK, Gtk.ResponseType.OK))
        self.pixbuf = None
        self.pixbuf_size = None
        self.parent = None
        self.rect = None
        self.margin = 30
        self.drag_target = 0 # the lines being dragged
        self.drag_info = DragInfo()
        self.line_color = (1.0, 0.4, 0)
        self.line_width = 2
        self.hot_threshold = 5
        self._cursor_pointer = None
        self._cursor_move = None
        self._cursor_origin = None
        self.surface = None
        self.anim_id = None

        self.load_ui()

    def set_pixbuf(self, pixbuf, rect=None):
        """Set a new pixbuf"""
        self.pixbuf = pixbuf
        self.pixbuf_size = (pixbuf.props.width, pixbuf.props.height)

        pwidth, pheight = self.pixbuf_size
        margin = self.margin
        if rect is None:
            self.rect = self.identity_rect
        else:
            self.rect = rect
            if rect.x0 > pwidth:
                rect.x0 = 0
            if rect.x1 > pwidth:
                rect.x1 = pwidth
            if rect.y0 > pheight:
                rect.y0 = 0
            if rect.y1 > pheight:
                rect.y1 = pheight

        # drawing area size
        width = pwidth + 2 * margin
        height = pheight + 2 * margin

        self.drag_target = 0
        self.dwg.set_size_request(width, height)
        self.queue_redraw(True)


    def load_ui(self):
        self.dwg = Gtk.DrawingArea()
        #self.dwg.props.can_focus = True

        emask = self.dwg.get_events()
        self.dwg.add_events(emask
                | Gdk.EventMask.BUTTON_PRESS_MASK
                | Gdk.EventMask.BUTTON_RELEASE_MASK
                | Gdk.EventMask.POINTER_MOTION_MASK
                )
        self.dwg.connect("map", self.on_map)
        self.dwg.connect("unmap", self.on_unmap)
        self.dwg.connect("draw", self.on_draw)
        self.dwg.connect('button-press-event', self.on_dwg_button_pressed)
        self.dwg.connect('button-release-event', self.on_dwg_button_released)
        self.dwg.connect('motion-notify-event', self.on_dwg_motion_notify)
        self.connect("response", self.on_response)
        self.connect("delete-event", self.close)

        box = self.get_content_area()
        box.add(self.dwg)
        box.show_all()

    @property
    def identity_rect(self):
        """a rectangle with the size equal pixbuf"""
        pwidth, pheight = self.pixbuf_size
        rect = fitz.Rect(0, 0, pwidth, pheight)
        return rect

    def on_response(self, wid, response_id):
        if response_id == Response.CLEAR:
            self.rect = self.identity_rect
            self.queue_redraw(True)
        else:
            self.close()

    def close(self, *args):
        self.hide()
        return True

    def on_map(self, wid):
        """ DrawingArea "map" event"""
        # create cursors for later use
        if self._cursor_pointer is None:
            gdkwin = wid.props.window
            self._cursor_origin = gdkwin.get_cursor()

            disp = gdkwin.get_display()
            self._cursor_pointer = Gdk.Cursor.new_from_name(disp, "pointer")
            self._cursor_move = Gdk.Cursor.new_from_name(disp, "move")
            gdkwin.set_cursor(self._cursor_pointer)

        anim_interval = 120
        self.anim_id = GLib.timeout_add(anim_interval, self.queue_redraw)

    def on_unmap(self, wid):
        """ DrawingArea "map" event"""
        if self.anim_id:
            GLib.source_remove(self.anim_id)
            self.anim_id = None

    def set_cursor(self, cursor):
        """set cursor if changed"""
        gdkwin = self.dwg.props.window
        cursor_now = gdkwin.get_cursor()
        if cursor_now != cursor:
            gdkwin.set_cursor(cursor)

    def set_rect_by_drag_info(self, rect):
        """set rectangle content by current dragging value"""
        if not self.drag_info.xy:
            return
        margin = self.margin
        x, y = self.drag_info.xy
        if self.drag_target & LineNum.H0:
            rect.y0 = y - margin
        if self.drag_target & LineNum.H1:
            rect.y1 = y - margin
        if self.drag_target & LineNum.V0:
            rect.x0 = x - margin
        if self.drag_target & LineNum.V1:
            rect.x1 = x - margin

    def render_page(self):
        '''We can render to a image surface and copy to dwg to speed up.'''
        pixbuf = self.pixbuf
        margin = self.margin

        if self.drag_target == 0:
            rect = self.rect
        else:
            rect0 = self.rect
            rect = fitz.Rect(rect0.x0, rect0.y0, rect0.x1, rect0.y1)
            self.set_rect_by_drag_info(rect)
            self.drag_info.updated = False

        pwidth, pheight = self.pixbuf_size

        # drawing area size
        width = pwidth + 2 * margin
        height = pheight + 2 * margin

        # load pixbuf into cairo surface
        self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
        cr = cairo.Context(self.surface)

        cr.save()
        Gdk.cairo_set_source_pixbuf(cr, pixbuf, margin, margin)
        cr.paint()
        cr.restore()

        # draw lines
        if rect:
            cr.save()
            cr.set_source_rgb(*self.line_color)
            cr.set_line_width(self.line_width)
            cr.set_operator(cairo.OPERATOR_SOURCE)

            # horizon lines
            for y in [rect.y0, rect.y1]:
                y = y + margin
                cr.move_to(0, y)
                cr.line_to(width, y)
                cr.stroke()

            # vertical lines
            for x in [rect.x0, rect.x1]:
                x = x + margin
                cr.move_to(x, 0)
                cr.line_to(x, height)
                cr.stroke()

            cr.restore()

    def point_on_line(self, x, y):
        """Test if a point is around any of indicating lines"""
        margin = self.margin
        hot_threshold = self.hot_threshold
        rect = self.rect

        target = 0
        if abs(rect.y0 + margin - y) <= hot_threshold:
            target |= LineNum.H0
        if abs(rect.y1 + margin - y) <= hot_threshold:
            target |= LineNum.H1
        if abs(rect.x0 + margin - x) <= hot_threshold:
            target |= LineNum.V0
        if abs(rect.x1 + margin - x) <= hot_threshold:
            target |= LineNum.V1
        return target

    def on_dwg_button_pressed(self, wid, evt):
        """mouse button press"""
        if not self.pixbuf:
            return

        # start drag if in dragging area
        target = self.point_on_line(evt.x, evt.y)
        if target != 0:
            self.set_cursor(self._cursor_move)
            if target != self.drag_target:
                self.drag_target = target

    def on_dwg_button_released(self, wid, evt):
        """mouse button release"""
        if self.drag_target != 0:
            self.set_rect_by_drag_info(self.rect)
            self.drag_target = 0
            self.set_cursor(self._cursor_move)
            return True
        else:
            self.set_cursor(self._cursor_origin)

    def on_dwg_motion_notify(self, wid, evt):
        """pointer move"""
        if not self.pixbuf:
            return

        pwidth, pheight = self.pixbuf_size
        if self.drag_target == 0:
            # test in dragging area
            target = self.point_on_line(evt.x, evt.y)
            if target != 0:
                self.set_cursor(self._cursor_pointer)
            else:
                self.set_cursor(self._cursor_origin)
        elif (self.margin <= evt.x <= self.margin + pwidth
                and self.margin <= evt.y <= self.margin + pheight) :
            # dragging, set drag_info
            self.drag_info.updated = True
            self.drag_info.xy = (evt.x, evt.y)

    def queue_redraw(self, force=False):
        """periodic redraw DrawingArea"""
        dinfo = self.drag_info
        if dinfo.updated or force:
            self.surface = None
            self.dwg.queue_draw()
        return True

    def on_draw(self, widget, cr):
        """on DrawingArea "draw" event"""""
        if not self.pixbuf:
            return
        if not self.surface:
            self.render_page()
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

