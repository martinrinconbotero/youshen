#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log

from gi.repository import GLib, Gdk

NATIVE = sys.getfilesystemencoding()

# http://stackoverflow.com/a/26457317
def InitSignal(on_interrupt_callback):
    """Add keyboard interrupt to gtk loop"""
    import signal
    def signal_action(signal):
        if signal == 1:
            log.debug("Caught signal SIGHUP(1)")
        elif signal == 2:
            log.debug("Caught signal SIGINT(2)")
        elif signal == 15:
            log.debug("Caught signal SIGTERM(15)")
        #gui.cleanup()
        on_interrupt_callback()

    def idle_handler(*args):
        log.debug("Python signal handler activated.")
        GLib.idle_add(signal_action, priority=GLib.PRIORITY_HIGH)

    def handler(*args):
        log.debug("GLib signal handler activated.")
        signal_action(args[0])

    def install_glib_handler(sig):
        unix_signal_add = None

        if hasattr(GLib, "unix_signal_add"):
            unix_signal_add = GLib.unix_signal_add
        elif hasattr(GLib, "unix_signal_add_full"):
            unix_signal_add = GLib.unix_signal_add_full

        if unix_signal_add:
            log.debug("Register GLib signal handler: %r" % sig)
            unix_signal_add(GLib.PRIORITY_HIGH, sig, handler, sig)
        else:
            log.debug("Can't install GLib signal handler, too old gi.")

    SIGS = [getattr(signal, s, None) for s in "SIGINT SIGTERM SIGHUP".split()]
    for sig in filter(None, SIGS):
        log.debug("Register Python signal handler: %r" % sig)
        signal.signal(sig, idle_handler)
        GLib.idle_add(install_glib_handler, sig, priority=GLib.PRIORITY_HIGH)

SIMPLE_TYPES = (bool, bytes, complex, float, int, str)
def clone_object(src, recursive_level=0):
    """Clone an object with all its public attributes"""
    prefix = "_youshen_clone_object_"
    max_level = 5  # max recursive level
    pname = prefix + type(src).__name__
    gnamespace = globals()
    if pname in gnamespace:
        ptype = gnamespace[pname]
    else:
        ptype = type(pname, (object,), {})
        gnamespace[pname] = ptype

    d = ptype()
    kind = getattr(src, "kind", None)
    for k in dir(src):
        if not k.startswith("_"):
            if kind == 1 and k in ["fileSpec", "dest"]:  # kind: 1 == GOTO
                v = None
            else:
                v = getattr(src, k)
            if type(v).__name__ == "SwigPyObject":
                continue
            if recursive_level < max_level and not (v is None or
                    isinstance(v, SIMPLE_TYPES) or not callable(v)):
                v = clone_object(v, recursive_level + 1)
            setattr(d, k, v)
    return d

def rgba_parse(color_str):
    """Return Gdk.RGBA from color string"""
    rgba = Gdk.RGBA()
    rgba.parse(color_str)
    return rgba

def marshall_variant(variant_type, val):
    """Marshall a python val to GLib.Variant"""
    if isinstance(val, GLib.Variant):
        return val
    if variant_type and variant_type.is_basic():
        val = GLib.Variant(variant_type.dup_string(), val)
    return val

def rect2dict(rect):
    """return a dict from Rect object"""
    d = {x: getattr(rect, x) for x in ["x", "y", "width", "height"]}
    return d

def pdf_time2datetime(ptime):
    """convert PDF time string to python datetime"""
    if ptime.startswith("D:"):
        ptime = ptime[2:]

    fmt = "%Y%m%d%H%M%S%z"
    if ptime.endswith("Z"):
        ptime = ptime.replace("Z", "+0000")
    ptime = ptime.replace("'", "")
    import datetime
    t = datetime.datetime.strptime(ptime, fmt)
    return t

class Singleton(type):
    """
    create a singleton:
        class MyClass(metaclass=Singleton):
            pass
    """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                    *args, **kwargs)
        return cls._instances[cls]

# See https://mail.python.org/pipermail/python-dev/2008-January/076194.html
def monkeypatch_method(cls, as_attr=None):
    """A decorator to add a single method to an existing class
    To use:

    from <somewhere> import <someclass>

    @monkeypatch_method(<someclass>)
    def <newmethod>(self, args):
        return <whatever>

    This adds <newmethod> to <someclass>
    """
    def decorator(func):
        aname = func.__name__ if as_attr is None else as_attr
        setattr(cls, aname, func)
        return func
    return decorator

def monkeypatch_class(name, bases, namespace):
    """
    A "metaclass" to add a number of methods (or other attributes)
    to an existing class, using a convenient class notation:

    To use:

    from <somewhere> import <someclass>

    class <newclass>(<someclass>, metaclass=monkeypatch_class):
        def <method1>(...): ...
        def <method2>(...): ...
        ...

    This adds <method1>, <method2>, etc. to <someclass>, and makes
    <newclass> a local alias for <someclass>.

    """
    assert len(bases) == 1, "Exactly one base class required"
    base = bases[0]
    for name, value in namespace.items():
        if name != "__metaclass__":
            setattr(base, name, value)
    return base

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

